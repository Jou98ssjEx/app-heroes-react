import React, { useContext } from 'react'
import { Navigate, useLocation } from 'react-router-dom';
import { AuthContext } from '../auth/authContext'

export const PrivateRoute = ({ children }) => {
    
    const { user } = useContext(AuthContext);

    const { pathname, search } = useLocation();
// console.log(location)
    localStorage.setItem('pathLast', pathname + search );
    // localStorage.setItem('pathLast', (location.search.length > 0 ) ? location.pathname + location.search : location.pathname);
    // localStorage.setItem('pathLast', location.search);
    
    return user.logged ? children : <Navigate to='/login'/>
}
