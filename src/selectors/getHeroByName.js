import { heroes } from "../data/heroes"

export const getHeroByName = ( hero = '' )=> { 

    // console.log('called')
    if( !hero){
        return [];
    }
    hero = hero.toLocaleLowerCase()
    return heroes.filter( h => h.superhero.toLocaleLowerCase().includes(hero))
//    return heroes;
} 