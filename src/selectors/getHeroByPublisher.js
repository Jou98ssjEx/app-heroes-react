import { heroes } from "../data/heroes";



export const getHeroByPublisher = ( publisher ) => {

    const data = ['DC Comics', 'Marvel Comics'];

    if( !data.includes(publisher)){
        throw new Error(`${publisher} don´t exist`)
    }

    return heroes.filter( hero => hero.publisher === publisher);
    
}