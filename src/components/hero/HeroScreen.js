import { useMemo } from "react";
import { Navigate, useNavigate, useParams } from "react-router-dom"
import { getHeroById } from "../../selectors/getHeroById";

export const HeroScreen = () => {

    // los arg vienen de los params usando Hook useParams
    const {heroId } = useParams();

    // memorizar el state de la propiedad del componente
    const hero = useMemo(() => getHeroById(heroId), [heroId]);

    // usar useNavigate para cambiar la url

    const navigate = useNavigate();
    // console.log(navigate())

    if(!hero){
        return <Navigate to={'/'}/>
    }

    const handleReturn = ( ) => {
        // para regresar una pag atras
        navigate(-1)
        // una forma de regresar
        // const [a, ] = heroId.split('-');
        // navigate(`/${a}`)


    }

    const { id, 
            alter_ego,
             characters,
             first_appearance,
             publisher,
             superhero } = hero;

    const imgPath = `/assets/${id}.jpg`

    return (
        <div className=" row mt-5">

            <div className="col-4">

                <img src={imgPath} alt={ superhero } className="img-thumbnail animate__animated animate__fadeInLeft" />

            </div>

            <div className="col-8 animate__animated animate__zoomIn">
                <h3> { superhero } </h3>
                <ul className="list-group list-group-flush"> 
                    <li className=" list-group-item">
                        <b>Alter ego:</b> { alter_ego }    
                    </li>
                    <li className=" list-group-item">
                        <b>Publisher:</b> { publisher }    
                    </li>
                    <li className=" list-group-item">
                        <b> First Appearance:</b> { first_appearance }    
                    </li>
                </ul>

                <h5 className=" mt-3"> Characters </h5>
                <p>{ characters } </p>

                <button
                    className="btn btn-outline-info"
                    onClick={ handleReturn }
                >
                    Return
                </button>
            </div>

            {/* <h1>HeroScreen</h1>

            <p> {superhero} </p> */}
        </div>
    )
}
