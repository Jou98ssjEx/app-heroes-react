import queryString from 'query-string'
import { useMemo } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

import { useForm } from "../../hooks/useFrom";
import { getHeroByName } from "../../selectors/getHeroByName";
import { HeroCard } from "../hero/HeroCard";

export const SearchScreen = () => {

    const navigate = useNavigate();
    const location = useLocation();

    const { q ='' } = queryString.parse(  location.search);

    // const initialForm = 

    const [ {searchHero}, handleInputChange, reset ] = useForm( {
        searchHero: q,
    } );

    let heroesFilter = useMemo(() => getHeroByName(q), [q]);
    
    const handleChange = (e) => {
        // e.preventDefaul();
        e.preventDefault();
        // h =  getHeroByName(searchHero);
        // console.log(searchHero);
        navigate(`?q=${searchHero}`)

        // reset()
    }

    return (
        <>
            <h1>Searchs</h1>
            <hr/>

            <div className="row">
                <div className="col-5">
                    <h4 className="text-info text-center" >Search Hero</h4>
                    <hr className="text-info text-center" />

                    <form  onSubmit={handleChange} >
                        <input
                            className="form-control mb-3"
                            type='text'
                            placeholder="Searching..."
                            autoComplete="off"
                            onChange={ handleInputChange}
                            name='searchHero'
                            value={ searchHero}
                        />

                        <button
                            className="btn btn-outline-info w-100"
                            type="submit"
                        >
                            Search
                        </button>
                        
                    </form>

                </div>

                <div className="col-7">
                    <h4 className="text-success text-center" >Result Heros</h4>
                    <hr className="text-success text-center" />

                    {
                        (q === '')
                            ? <div className='alert alert-info'> Searching Hero </div>
                            : (heroesFilter.length === 0)
                                && <div className='alert alert-danger'> Hero no found: {q} </div>
                    }

                    { heroesFilter.map( h => (
                        <HeroCard 
                            key={h.id}
                            {...h}                        
                        />

                    ))}
                    
                </div>

            </div>   
        </>
    )
}