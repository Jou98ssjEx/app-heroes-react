import { HeroList } from "../hero/HeroList"

export const DcScreen = () => {
    return (
        <>
            <h1 className="mb-2">Dc Screen</h1>  
            <hr/>

            <HeroList publisher={'DC Comics'} />

        </>
    )
}
