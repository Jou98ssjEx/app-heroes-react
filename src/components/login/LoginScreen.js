import { useContext } from "react";
import { useNavigate } from "react-router-dom"
import { AuthContext } from "../../auth/authContext";
import { types } from "../../types/types";

export const LoginScreen = () => {

    const { dispatch } = useContext(AuthContext);
    // console.log(dispatch);

    const navigate = useNavigate();
    const handleLogin = () =>{

        // console.log('hola')
        const action = {
            type: types.login,
            payload:{
                name:'Barry Allen',
                // logged: true
            }
        }

        dispatch(action);

        const pathLast = localStorage.getItem('pathLast') || '/marvel';

        navigate(pathLast, {
            // para volver atras
            replace:true
        })


    }

    return (
        <div className="container mt-4">
            <h1>LoginScreen</h1>
            <hr/>

            <button
                className="btn btn-outline-primary"
                onClick={handleLogin}
            >
                Login
            </button>

        </div>

    )
}