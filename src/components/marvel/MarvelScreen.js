import { HeroList } from "../hero/HeroList"

export const MarvelScreen = () => {
    return (
        <>
            <h1 className="mb-2">MarvelScreen</h1>   
            <hr/>

            <HeroList publisher={'Marvel Comics'} />
        </>
    )
}