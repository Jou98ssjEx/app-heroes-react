import React, { useContext } from 'react'
import { Link, NavLink, useNavigate } from 'react-router-dom'
import { AuthContext } from '../../auth/authContext';
import { types } from '../../types/types';

export const Navbar = () => {

    const {user, dispatch } = useContext(AuthContext);
    // console.log(dispatch)
    const navigate = useNavigate();

    const handleLogout = ( ) => {

        const action = {
            type: types.logout
        }

        dispatch(action)
        // console.log('Logout')
        navigate('/login', {replace: true});
    }

    return (
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
            
            <Link 
                className="navbar-brand ml-2" 
                to="/"
            >
                <b className='m-3'> Heroes </b>
            </Link>

            <div className="navbar-collapse">
                <div className="navbar-nav">

                    <NavLink 
                        // activeClassName="active"
                        // className= { ({ isActive } ) =>  'nav-item nav-link' + ( isActive ? 'active': '') }
                        className="nav-item nav-link" 
                        // exact
                        to="/marvel"
                    >
                        Marvel
                    </NavLink>

                    <NavLink 
                        // activeClassName="active"
                        className="nav-item nav-link" 
                        // className= { ({ isActive } ) =>  'nav-item nav-link' + ( isActive ? 'active': '') }

                        // exact
                        to="/dc"
                    >
                        DC
                    </NavLink>

                    <NavLink 
                        className="nav-item nav-link" 
                        to="/search"
                    >
                        Search
                    </NavLink>
                </div>
            </div>

            <div className=" navbar-collapse collapse w-100 order-3 dual-collapse2 d-flex justify-content-end">
                <ul className=" mr-2 navbar-nav ml-auto">

                    <span className='nav-item nav-link text-info'>
                        {/* User */}
                       { user.name}
                    </span>

                    <button 
                        // activeClassName="active"
                        className="nav-item nav-link btn" 
                        onClick={handleLogout}
                        // exact
                        // to="/login"
                    >
                       <b className='m-2'>
                       Logout
                        </b> 
                    </button>
                </ul>
            </div>
        </nav>
    )
}