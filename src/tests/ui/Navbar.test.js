import { mount } from "enzyme"
import { MemoryRouter, Route, Routes } from "react-router-dom"
import { AuthContext } from "../../auth/authContext"
import { Navbar } from "../../components/ui/Navbar"
import { types } from "../../types/types";



const mockNavigate = jest.fn();

// el segundo parametro retona una funcion, para cambiar y simular el useNavigate
jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    // simular el hook, que retorne otra funcion
    useNavigate: () => mockNavigate
}))


describe('evidence in <Navbar />', () => {
    
    const valueContext = {
        dispatch: jest.fn(),
        user:{
            logged: true,
            name: 'Barry Allen'
        }
    }

    const wrapper = mount(
        <AuthContext.Provider value={valueContext} >
            <MemoryRouter initialEntries={ ['/']}>
                <Routes>
                    <Route path='/' element={ <Navbar />} />
                </Routes>
            </MemoryRouter>
        </AuthContext.Provider>
    )


    test('should show successfy', () => {
        

        // const wrapper = mount(
        //     <AuthContext.Provider value={valueContext} >
        //         <MemoryRouter >
        //             <Navbar/>
        //         </MemoryRouter>
        //     </AuthContext.Provider>
        // )

        expect(wrapper.find('.text-info').text().trim()).toBe(valueContext.user.name)
        expect(wrapper).toMatchSnapshot();
    });

    test('should call logout, navigate and dispatch with args', () => {
        
        // const wrapper = mount(
        //     <AuthContext.Provider value={valueContext} >
        //         <MemoryRouter >
        //             <Navbar/>
        //         </MemoryRouter>
        //     </AuthContext.Provider>
        // )

        wrapper.find('button').simulate('click');

        expect(valueContext.dispatch).toHaveBeenCalledWith({ 'type': types.logout })
        // console.log(mockNavigate).to
        expect(mockNavigate).toHaveBeenCalledWith('/login', {replace: true})
    })
    
    
})
