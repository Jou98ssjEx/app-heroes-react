import { mount } from "enzyme"
import { MemoryRouter, Route, Routes } from "react-router-dom"
import { HeroScreen } from "../../../components/hero/HeroScreen"

const mockNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useNavigate: () => mockNavigate
}))

describe('evidence in <HeroScreen/>', () => {
    
    test('shouldn´t show heroScreen without hero  ', () => {
        
        const wrapper = mount(
            <MemoryRouter initialEntries={['/hero']}>
                <Routes>
                    <Route path='/hero' element={<HeroScreen/>} />
                    <Route path='/' element={<h1>Don´t page of Hero</h1>} />
                </Routes>
            </MemoryRouter>
        )

        // console.log(wrapper.html())
        expect(wrapper.find('h1').text().trim()).toBe('Don´t page of Hero')

    })


    test('should show heroScreen with hero  ', () => {
        
        const wrapper = mount(
            <MemoryRouter initialEntries={['/hero/marvel-spider']}>
                <Routes>
                    <Route path='/hero/:heroId' element={<HeroScreen/>} />
                    {/* <Route path='/' element={<h1>Don´t page of Hero</h1>} />KC */}
                </Routes>
            </MemoryRouter>
        )

        // console.log(wrapper.html())
        expect(wrapper.find('.row').exists()).toBe(true)

    })
    
    test('should show heroScreen with hero  ', () => {
        
        const wrapper = mount(
            <MemoryRouter initialEntries={['/hero/marvel-spider']}>
                <Routes>
                    <Route path='/hero/:heroId' element={<HeroScreen/>} />
                    {/* <Route path='/' element={<h1>Don´t page of Hero</h1>} />KC */}
                </Routes>
            </MemoryRouter>
        )

        wrapper.find('button').simulate('click');

        expect(mockNavigate).toHaveBeenCalledWith(-1)
    })
   
    test('should show heroScreen with hero  ', () => {
        
        const wrapper = mount(
            <MemoryRouter initialEntries={['/hero/marvel-spider12233']}>
                <Routes>
                    <Route path='/hero/:heroId' element={<HeroScreen/>} />
                    <Route path='/' element={<h1>Don´t page of Hero</h1>} />KC
                </Routes>
            </MemoryRouter>
        )

        expect(wrapper.text()).toBe('Don´t page of Hero')

    })


})
