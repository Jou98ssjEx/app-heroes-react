const { mount } = require("enzyme")
const { MemoryRouter } = require("react-router-dom");
const { SearchScreen } = require("../../../components/search/SearchScreen");


const mockNavigate = jest.fn();

// el segundo parametro retona una funcion, para cambiar y simular el useNavigate
jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    // simular el hook, que retorne otra funcion
    useNavigate: () => mockNavigate
}))

describe('evidence in <SearchScreen/>', () => {
    

    test('should show successfy', () => {
        
        const wrapper = mount(
            <MemoryRouter initialEntries={ ['/search']}>
                <SearchScreen />
            </MemoryRouter>
        )
    
        // console.log(wrapper)
        expect(wrapper).toMatchSnapshot();
        expect(wrapper.find('.alert-info').text().trim()).toBe('Searching Hero')
        // expect(wrapper.find('.text-info').exists()).toBe(true)
        
    });

    test('should show Batmat in component', () => {
        
        const wrapper = mount(
            <MemoryRouter initialEntries={ ['/search?q=batman']}>
                <SearchScreen />
            </MemoryRouter>
        );

        expect(wrapper).toMatchSnapshot();
        // evaluar que la caja de texto tenga el query params
        expect(wrapper.find('input').prop('value')).toBe('batman')
    });

    test('should show the alert of Batman123 in params', () => {
        
        const wrapper = mount(
            <MemoryRouter initialEntries={ ['/search?q=batman123'] }>
                <SearchScreen/>
            </MemoryRouter>
        )

        expect(wrapper.find('.alert-danger').text().trim()).toBe('Hero no found: batman123')
    })
    

    test('should call this navigate an new windows', () => {
        const hero = 'super'
        
        const wrapper = mount(
            <MemoryRouter initialEntries={ [`/search?q=${hero}`] }>
                <SearchScreen/>
            </MemoryRouter>
        )

        // Simular la caja de texto con el enter
        wrapper.find('input').simulate('change', { target:{
            name: 'searchHero',
            value: hero
        }});

        // Simular el onSumit del formulario // y llamr la funcion autoinvocada
        wrapper.find('form').prop('onSubmit') ({ preventDefault(){}});


        // la compararion de la ruta con el parametro del input
        expect(mockNavigate).toHaveBeenCalledWith(`?q=${hero}`)
    })
    
    
})
