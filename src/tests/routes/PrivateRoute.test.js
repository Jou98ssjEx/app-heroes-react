import { mount } from "enzyme"
import { MemoryRouter } from "react-router-dom"
import { AuthContext } from "../../auth/authContext"
import { PrivateRoute } from "../../routers/PrivateRoute"

// const mockNavigate = jest.fn()

jest.mock('react-router-dom', ( ) => ({
    ...jest.requireActual('react-router-dom'),
    Navigate: () => <span>Saliendo...</span>
}))

describe('evidence in <PrivateRouter />', () => {
    

    // llama al local storage
    Storage.prototype.setItem = jest.fn();

    test('should show the component if authtenticate and save in localStorage', () => {
        

        const valueContext = {
            user:{
                name: 'Barry Allen',
                logged: true
            }
        }

        const wrapper = mount(
            <AuthContext.Provider value={valueContext}>
                <MemoryRouter>
                    <PrivateRoute>
                        <h1>Private Router</h1>
                    </PrivateRoute>
                </MemoryRouter>
            </AuthContext.Provider>
        )

        expect(wrapper.text().trim()).toBe('Private Router');
        expect(localStorage.setItem).toHaveBeenCalledWith("pathLast", "/")

    })
    
    test('should show the component saliendo', () => {
        

        const valueContext = {
            user:{
                // name: 'Barry Allen',
                logged: false
            }
        }

        const wrapper = mount(
            <AuthContext.Provider value={valueContext}>
                <MemoryRouter initialEntries={['/']}>
                    <PrivateRoute>
                        <h1>Private Router</h1>
                    </PrivateRoute>
                </MemoryRouter>
            </AuthContext.Provider>
        )

        expect(wrapper.text().trim()).toBe('Saliendo...');
        // expect(localStorage.setItem).toHaveBeenCalledWith("pathLast", "/")

    })
    

})
