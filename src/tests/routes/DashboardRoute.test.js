import { mount } from "enzyme"
import { AuthContext } from "../../auth/authContext"
import { MemoryRouter} from 'react-router-dom'
import { DashboardRoute } from "../../routers/DashboardRoute"


describe('evidence in <DashboardRouter />', () => {
    
   test('should successfy in router for default', () => {
       
        const valueContext = {
            user:{
                logged: true,
                name: 'Barry Allen'
            }
        }

        const wrapper = mount(
            <AuthContext.Provider value={valueContext}>
                // se necesita el memory router por el uso del useNavigate en el componete
                <MemoryRouter initialEntries={['/']}>
                    <DashboardRoute />
                </MemoryRouter>
            </AuthContext.Provider>
        )

        expect(wrapper).toMatchSnapshot();

        expect(wrapper.find('.text-info').text().trim()).toBe(valueContext.user.name)
   })
  
   test('should successfy in router /dc', () => {
       
        const valueContext = {
            user:{
                logged: true,
                name: 'Barry Allen'
            }
        }

        const wrapper = mount(
            <AuthContext.Provider value={valueContext}>
                // se necesita el memory router por el uso del useNavigate en el componete
                <MemoryRouter initialEntries={['/dc']}>
                    <DashboardRoute />
                </MemoryRouter>
            </AuthContext.Provider>
        )

        expect(wrapper).toMatchSnapshot();
        expect(wrapper.find('h1').text().trim()).toBe('Dc Screen')
   })
    
})
