const { mount } = require("enzyme")
const { AuthContext } = require("../../auth/authContext")
const { AppRoute } = require("../../routers/AppRoute")

describe('evidenve in <AppRouter /> ', () => {
    
    const valueContext = {
        user: {
            logged: false
        }
    }

    test('should show login if is authentice', () => {
        
        const wrapper = mount(
            <AuthContext.Provider value={valueContext}>
                <AppRoute/>
            </AuthContext.Provider>
        )

        expect(wrapper).toMatchSnapshot();
        expect(wrapper.find('h1').text().trim()).toBe('LoginScreen');
    })

    test('should the component Marvel when is authentice', () => {
        
        const valueContext = {
            user:{
                logged: true,
                name: 'Barry Allen'
            }
        }

        const wrapper = mount(
            <AuthContext.Provider value={valueContext}>
                <AppRoute/>
            </AuthContext.Provider>
        )

        expect(wrapper).toMatchSnapshot()
        expect(wrapper.find('nav').exists()).toBe(true);
    })
    
    
})
