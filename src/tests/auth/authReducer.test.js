import { authReducer } from "../../auth/authReducer"
import { types } from "../../types/types"


describe('file evidence the auth Reducer', () => {

    test('should return state for default', () => {
        
       const state = authReducer({ logged: false }, {} );
        
       expect(state).toEqual({ logged: false});

    });

    test('should authenticate and place the "name" for user' , () => {
        
        const action = {
            type: types.login,
            payload:{
                name: 'Barry Allen'
            }
        }

        const state = authReducer({logged: false}, action);

        expect(state).toEqual({logged:true, name: 'Barry Allen'})
    })
    
    test('should delete "name" of user and logged: false', () => {
        
        const action = {
            type: types.logout
        }

        const state = authReducer( {logged: true, name: 'Barry Allen'}, action);

        expect(state).toEqual({logged: false})
    })
    
    

})
