import { mount } from "enzyme"
import { MemoryRouter, Route, Routes } from "react-router-dom"
import { AuthContext } from "../../auth/authContext"
import { LoginScreen } from "../../components/login/LoginScreen"
import { types } from "../../types/types"

const mockNavigate = jest.fn();

jest.mock('react-router-dom', ()=> ({
    ...jest.requireActual('react-router-dom'),
    useNavigate: ()=> mockNavigate
}))


describe('evidence in <LoginScreen/>', () => {
    
    const valueContext = {
        dispatch: jest.fn(),
        user:{
            // name: 'Barry Allen',
            logged: false
        }
    }

    const wrapper = mount(
        <AuthContext.Provider value={valueContext} >
            <MemoryRouter initialEntries={ ['/login']}>
                <Routes>
                    <Route path='/login' element={ <LoginScreen />} />
                </Routes>
            </MemoryRouter>
        </AuthContext.Provider>
    )
    // const wrapper = mount(
    //     <AuthContext.Provider value={valueContext}>
    //         <MemoryRouter>
    //             <LoginScreen/>

    //         </MemoryRouter>
    //     </AuthContext.Provider>
    // )

    test('should has macth with snapshot', () => {
        

        expect(wrapper).toMatchSnapshot();
        // console.log(wrapper.html())
    })

    test('should make dispatch and navigate', () => {
        
        wrapper.find('button').simulate('click')
        // console.log(wrapper)
        expect(valueContext.dispatch).toHaveBeenCalledWith({payload: { name: 'Barry Allen'}, type: types.login })

        // mock del mavigate
        expect(mockNavigate).toHaveBeenCalledWith("/marvel", {"replace": true})

        // guardar otra ruta en el local storage
        localStorage.setItem('pathLast', '/dc')

        // simular de nuevo el click
        wrapper.find('button').simulate('click')

        // evaluear la ruta
        expect(mockNavigate).toHaveBeenCalledWith("/dc", {"replace": true})


    })
    
    
})
