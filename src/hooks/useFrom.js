
import { useState } from 'react'

export const useForm = (initialState ={}) => {


    const [form, setForm] = useState(initialState);

    const reset = () =>{
        setForm(initialState);
    }

    const hadleChangeInput = ( { target } ) => {
        // if( target.value.length < 1){
        //     console.log('0')
        //     throw new Error(`${target.name} is required`)
        // }
        setForm( {
            ...form,
            [target.name] : target.value
        })
    }

    return [form, hadleChangeInput, reset]
}



